FROM ubuntu:20.04

# Desactivation du reglage creneaux horaire
ARG DEBIAN_FRONTEND=noninteractive

# Mise à jour ubuntu
RUN apt-get -y update \
&& apt-get -y upgrade

# Installation de docker
RUN apt install -y docker.io

# Installation de docker-compse
RUN apt install -y docker-compose
